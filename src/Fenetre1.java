import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

 
 
public class Fenetre1 extends BorderPane {
    
    private Button btnOnline;
    private Button btnLocal;
    
    public Fenetre1(Button btnOnline, Button btnLocal){
        super();
        this.btnOnline = btnOnline;
        this.btnLocal = btnLocal;
        
        this.setStyle("-fx-background-color: #ffec9f;");
        this.setPadding(new Insets(10));
        this.setTop(this.enHaut());   
        this.setCenter(this.uneVBox());
    }
    
    private Pane enHaut(){
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(20,20,20,10));        
        Text title = new Text("PUISSANCE 4");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 32));
        hbox.getChildren().add(title);
        hbox.setStyle("-fx-background-color: #fea0a0;");
        return hbox;
    }

    private VBox uneVBox() {
        VBox vbox = new VBox();
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(20));
        
        Text option1 = new Text("2 JOUEURS");
        option1.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        HBox choix1 = new HBox();
        btnOnline.setStyle("-fx-background-color: #fea0a0;");
        btnOnline.setPadding(new Insets(20));
        btnLocal.setStyle("-fx-background-color: #fea0a0;");
        btnLocal.setPadding(new Insets(20));
        choix1.getChildren().addAll(btnOnline, btnLocal);
        vbox.getChildren().addAll(option1, choix1);      
        return vbox;
    }
}
