import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


 
public class AppliPuissance4 extends Application {
    
    private Button btnOnline;
    private Button btnLocal;
    private Button btnQuitter;
    private Scene scene;
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(AppliPuissance4.class, args);
    }
    
    @Override
    public void init(){
        this.btnOnline = new Button("ONLINE");        
        this.btnLocal = new Button("LOCAL");

        //ControleBouton controleur = new ControleBouton(this);        
        //this.btnOnline.setOnAction(controleur);
        //this.btnLocal.setOnAction(controleur);
        //this.btnQuitter.setOnAction(controleur);
        
    }
    
    @Override
    public void start(Stage stage) {
        Pane root = new Fenetre1(this.btnOnline, this.btnLocal);
        this.scene = new Scene(root, 900, 700);
        stage.setScene(scene);
        stage.setTitle("Appli Puissance4");
        stage.show();
    }
 
    public void afficheFenetre1(){
        Pane root = new Fenetre1(this.btnOnline, this.btnLocal);
        this.scene.setRoot(root);
    }
    
    /*public void afficheFenetre2(){
        Pane root = new Fenetre2(this.btnQuitter);    
        this.scene.setRoot(root);
    }*/
}